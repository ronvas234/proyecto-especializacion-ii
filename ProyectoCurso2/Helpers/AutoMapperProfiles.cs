﻿using AutoMapper;
using ProyectoCurso2.Entities;
using ProyectoCurso2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCurso2.Helpers
{
    public class AutoMapperProfiles: Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<ClientesModel, Clientes>().ReverseMap();
            CreateMap<ProveedoresModel, Proveedores>().ReverseMap();
            CreateMap<VendedorModel, Vendedor>().ReverseMap();
            CreateMap<InventariosModel, Inventarios>().ReverseMap();
            CreateMap<ProductosModel, Productos>().ReverseMap();
        }
    }
}
