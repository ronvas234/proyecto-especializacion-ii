﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCurso2.Entities
{
    public class Productos
    {
        [Key]
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public Decimal Precio { get; set; }
        [ForeignKey("IdProducto")]
        public virtual List<Inventarios> Inventarios { get; set; }

    }
}
