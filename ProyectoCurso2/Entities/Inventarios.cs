﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCurso2.Entities
{
    public class Inventarios
    {
        [Key]
        public int Id { get; set; }
        public int IdProducto { get; set; }
        public Decimal stockentrada { get; set; }
        public Decimal stocksalida { get; set; }
        public Decimal total { get; set; }
        public virtual Productos Productos { get; set; }

    }
}
