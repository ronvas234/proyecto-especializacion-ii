﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCurso2.Entities
{
    public class Vendedor
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
