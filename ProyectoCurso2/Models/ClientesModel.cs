﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCurso2.Models
{
    public class ClientesModel
    {
        [Key]
        [Display(Name = "Id:")]
        public int Id { get; set; }
        [Display(Name = "Nombre:")]
        [Required(ErrorMessage = "El campo nombre es obligatorio")]
        [StringLength(100, ErrorMessage = "No puede ingresar más de 100 cáracteres")]
        [RegularExpression("^[A-Z a-z]*$", ErrorMessage = "Nombre inválido")]
        public string Nombre { get; set; }
        [Display(Name = "Fecha Nacimiento")]
        [Required(ErrorMessage = "El campo fecha nacimiento  es obligatorio")]
        [DataType(DataType.DateTime)]
        public DateTime FechaNacimiento { get; set; }
    }
}
