﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCurso2.Models
{
    public class ProductosModel
    {
        [Key]
        [Display(Name = "Id:")]
        public int Id { get; set; }
        [Display(Name = "Descripción:")]
        [Required(ErrorMessage = "El campo nombre es obligatorio")]
        [StringLength(100, ErrorMessage = "No puede ingresar más de 100 cáracteres")]
        [RegularExpression("^[A-Z a-z 0-9]*$", ErrorMessage = "Descripción inválida")]
        public string Descripcion { get; set; }
        [Display(Name = "Precio:")]
        [Required(ErrorMessage = "El campo precio es obligatorio")]
        [DataType(DataType.Currency)]
        public Decimal Precio { get; set; }
        [ForeignKey("IdProducto")]
        public virtual List<InventariosModel> Inventarios { get; set; }
    }
}
