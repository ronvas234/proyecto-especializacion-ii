﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCurso2.Models
{
    public class InventariosModel
    {
        [Key]
        [Display(Name = "Id:")]
        public int Id { get; set; }
        [Display(Name = "Id Producto:")]
        [Required(ErrorMessage = "El campo id producto es obligatorio")]
        public int IdProducto { get; set; }
        [Display(Name = "Stock Entrada:")]
        [Required(ErrorMessage = "El campo stock entrada es obligatorio")]
        [DataType(DataType.Currency)]
        public Decimal stockentrada { get; set; }
        [Display(Name = "Stock Salida:")]
        [Required(ErrorMessage = "El campo stock salida es obligatorio")]
        [DataType(DataType.Currency)]
        public Decimal stocksalida { get; set; }
        [Display(Name = "Total:")]
        [Required(ErrorMessage = "El campo total es obligatorio")]
        [DataType(DataType.Currency)]
        public Decimal total { get; set; }
        public virtual ProductosModel Productos { get; set; }
    }
}
