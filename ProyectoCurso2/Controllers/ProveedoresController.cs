﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoCurso2.Data;
using ProyectoCurso2.Entities;
using ProyectoCurso2.Models;

namespace ProyectoCurso2.Controllers
{
    public class ProveedoresController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly SignInManager<IdentityUser> SignInManager;
        private readonly UserManager<IdentityUser> UserManager;

        public ProveedoresController(ApplicationDbContext context, IMapper mapper, SignInManager<IdentityUser> _sign, UserManager<IdentityUser> _user)
        {
            _context = context;
            _mapper = mapper;
            SignInManager = _sign;
            UserManager = _user;
        }

        // GET: Proveedores
        public async Task<ActionResult> Index()
        {
            if (SignInManager.IsSignedIn(User))
            {
                var query = await _context.Proveedores.ToListAsync();
                var list = _mapper.Map<List<ProveedoresModel>>(query);
                return View(list);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Proveedores/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Proveedores
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var proveedoresModel = _mapper.Map<ProveedoresModel>(query);
                return View(proveedoresModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Proveedores/Create
        public ActionResult Create()
        {
            if (SignInManager.IsSignedIn(User))
            {
               return View();
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Proveedores/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Id,Nombre")] ProveedoresModel proveedoresModel)
        {
            if (ModelState.IsValid)
            {
                var proveedores = _mapper.Map<Proveedores>(proveedoresModel);
                _context.Add(proveedores);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(proveedoresModel);
        }

        // GET: Proveedores/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Proveedores.FindAsync(id);
                if (query == null)
                {
                    return NotFound();
                }
                var proveedoresModel = _mapper.Map<ProveedoresModel>(query);
                return View(proveedoresModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Proveedores/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, [Bind("Id,Nombre")] ProveedoresModel proveedoresModel)
        {
            if (id != proveedoresModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var proveedores = _mapper.Map<Proveedores>(proveedoresModel);
                    _context.Update(proveedores);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProveedoresExists(proveedoresModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(proveedoresModel);
        }

        // GET: Proveedores/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Proveedores
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var proveedoresModel = _mapper.Map<ProveedoresModel>(query);
                return View(proveedoresModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Proveedores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var proveedores = await _context.Proveedores.FindAsync(id);
            _context.Proveedores.Remove(proveedores);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProveedoresExists(int id)
        {
            return _context.Proveedores.Any(e => e.Id == id);
        }
    }
}
