﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoCurso2.Data;
using ProyectoCurso2.Entities;
using ProyectoCurso2.Models;

namespace ProyectoCurso2.Controllers
{
    public class ClientesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly SignInManager<IdentityUser> SignInManager;
        private readonly UserManager<IdentityUser> UserManager;

        public ClientesController(ApplicationDbContext context,IMapper mapper, SignInManager<IdentityUser> _sign, UserManager<IdentityUser> _user)
        {
            _context = context;
            _mapper = mapper;
            SignInManager = _sign;
            UserManager = _user;
        }

        // GET: Clientes
        public async Task<ActionResult> Index()
        {
            if (SignInManager.IsSignedIn(User))
            {
                var query = await _context.Clientes.ToListAsync();
                var list = _mapper.Map<List<ClientesModel>>(query);
                return View(list);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Clientes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Clientes
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var clientesModel = _mapper.Map<ClientesModel>(query);
                return View(clientesModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            if (SignInManager.IsSignedIn(User))
            {
                return View();
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }
        public async Task<ActionResult> FirstClients()
        {
            if (SignInManager.IsSignedIn(User))
            {
                var query = await _context.Clientes.OrderBy(x => x.Id).FirstAsync();
                if (query == null)
                {
                    return NotFound();
                }
                var clientesModel = _mapper.Map<ClientesModel>(query);
                return View(clientesModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Id,Nombre,FechaNacimiento")] ClientesModel clientesModel)
        {
            if (ModelState.IsValid)
            {
                var clientes = _mapper.Map<Clientes>(clientesModel);
                _context.Add(clientes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(clientesModel);
        }

        // GET: Clientes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {

            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Clientes.FindAsync(id);
                if (query == null)
                {
                    return NotFound();
                }
                var clientesModel = _mapper.Map<ClientesModel>(query);
                return View(clientesModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, [Bind("Id,Nombre,FechaNacimiento")] ClientesModel clientesModel)
        {
            if (id != clientesModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var clientes = _mapper.Map<Clientes>(clientesModel);
                    _context.Update(clientes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientesExists(clientesModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(clientesModel);
        }

        // GET: Clientes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Clientes
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var clientesModel = _mapper.Map<ClientesModel>(query);
                return View(clientesModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var clientes = await _context.Clientes.FindAsync(id);
            _context.Clientes.Remove(clientes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClientesExists(int id)
        {
            return _context.Clientes.Any(e => e.Id == id);
        }
    }
}
