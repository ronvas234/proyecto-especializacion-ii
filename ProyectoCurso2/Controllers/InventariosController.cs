﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoCurso2.Data;
using ProyectoCurso2.Entities;
using ProyectoCurso2.Models;

namespace ProyectoCurso2.Controllers
{
    public class InventariosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly SignInManager<IdentityUser> SignInManager;
        private readonly UserManager<IdentityUser> UserManager;
        public InventariosController(ApplicationDbContext context, IMapper mapper, SignInManager<IdentityUser> _sign, UserManager<IdentityUser> _user)
        {
            _context = context;
            _mapper = mapper;
            SignInManager = _sign;
            UserManager = _user;
        }

        // GET: Inventarios
        public async Task<ActionResult> Index()
        {
            if (SignInManager.IsSignedIn(User))
            {
                var query = _context.Inventarios.Include(i => i.Productos);
                var list = _mapper.Map<List<InventariosModel>>(await query.ToListAsync());
                return View(list);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Inventarios/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Inventarios
                    .Include(i => i.Productos)
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var inventariosModel = _mapper.Map<InventariosModel>(query);
                return View(inventariosModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Inventarios/Create
        public ActionResult Create()
        {
            if (SignInManager.IsSignedIn(User))
            {
                ViewData["IdProducto"] = new SelectList(_context.Set<Productos>(), "Id", "Descripcion");
                return View();
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Inventarios/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Id,IdProducto,stockentrada,stocksalida,total")] InventariosModel inventariosModel)
        {
            if (ModelState.IsValid)
            {
                var inventarios = _mapper.Map<Inventarios>(inventariosModel);
                _context.Add(inventarios);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdProducto"] = new SelectList(_context.Set<Productos>(), "Id", "Descripcion", inventariosModel.IdProducto);
            return View(inventariosModel);
        }

        // GET: Inventarios/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Inventarios.FindAsync(id);
                if (query == null)
                {
                    return NotFound();
                }
                var inventariosModel = _mapper.Map<InventariosModel>(query);
                ViewData["IdProducto"] = new SelectList(_context.Set<Productos>(), "Id", "Descripcion", inventariosModel.IdProducto);
                return View(inventariosModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Inventarios/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, [Bind("Id,IdProducto,stockentrada,stocksalida,total")] InventariosModel inventariosModel)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id != inventariosModel.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        var inventarios = _mapper.Map<Inventarios>(inventariosModel);
                        _context.Update(inventarios);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!InventariosExists(inventariosModel.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                ViewData["IdProducto"] = new SelectList(_context.Set<Productos>(), "Id", "Descripcion", inventariosModel.IdProducto);
                return View(inventariosModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Inventarios/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Inventarios
                    .Include(i => i.Productos)
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var inventariosModel = _mapper.Map<InventariosModel>(query);
                return View(inventariosModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Inventarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var inventarios = await _context.Inventarios.FindAsync(id);
            _context.Inventarios.Remove(inventarios);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InventariosExists(int id)
        {
            return _context.Inventarios.Any(e => e.Id == id);
        }
    }
}
