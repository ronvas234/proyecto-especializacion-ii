﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoCurso2.Data;
using ProyectoCurso2.Entities;
using ProyectoCurso2.Models;

namespace ProyectoCurso2.Controllers
{
    public class VendedoresController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly SignInManager<IdentityUser> SignInManager;
        private readonly UserManager<IdentityUser> UserManager;

        public VendedoresController(ApplicationDbContext context, IMapper mapper, SignInManager<IdentityUser> _sign, UserManager<IdentityUser> _user)
        {
            _context = context;
            _mapper = mapper;
            SignInManager = _sign;
            UserManager = _user;
        }

        // GET: Vendedores
        public async Task<ActionResult> Index()
        {
            if (SignInManager.IsSignedIn(User))
            {
                var query = await _context.Vendedor.ToListAsync();
                var list = _mapper.Map<List<VendedorModel>>(query);
                return View(list);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Vendedores/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Vendedor
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var vendedorModel = _mapper.Map<VendedorModel>(query);
                return View(vendedorModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Vendedores/Create
        public ActionResult Create()
        {
            if (SignInManager.IsSignedIn(User))
            {
                return View();
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Vendedores/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Id,Nombre")] VendedorModel vendedorModel)
        {
            if (ModelState.IsValid)
            {
                var vendedor = _mapper.Map<Vendedor>(vendedorModel);
                _context.Add(vendedor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(vendedorModel);
        }

        // GET: Vendedores/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Vendedor.FindAsync(id);
                if (query == null)
                {
                    return NotFound();
                }
                var vendedorModel = _mapper.Map<Vendedor>(query);
                return View(vendedorModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Vendedores/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, [Bind("Id,Nombre")] VendedorModel vendedorModel)
        {
            if (id != vendedorModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var vendedor = _mapper.Map<Vendedor>(vendedorModel);
                    _context.Update(vendedor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VendedorExists(vendedorModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vendedorModel);
        }

        // GET: Vendedores/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Vendedor
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var vendedorModel = _mapper.Map<VendedorModel>(query);
                return View(vendedorModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Vendedores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vendedor = await _context.Vendedor.FindAsync(id);
            _context.Vendedor.Remove(vendedor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VendedorExists(int id)
        {
            return _context.Vendedor.Any(e => e.Id == id);
        }
    }
}
