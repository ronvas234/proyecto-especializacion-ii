﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoCurso2.Data;
using ProyectoCurso2.Entities;
using ProyectoCurso2.Models;

namespace ProyectoCurso2.Controllers
{
    public class ProductosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly SignInManager<IdentityUser> SignInManager;
        private readonly UserManager<IdentityUser> UserManager;

        public ProductosController(ApplicationDbContext context, IMapper mapper, SignInManager<IdentityUser> _sign, UserManager<IdentityUser> _user)
        {
            _context = context;
            _mapper = mapper;
            SignInManager = _sign;
            UserManager = _user;
        }

        // GET: Productos
        public async Task<ActionResult> Index()
        {
            if (SignInManager.IsSignedIn(User))
            {
                var query = await _context.Productos.ToListAsync();
                var list = _mapper.Map<List<ProductosModel>>(query);
                return View(list);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Productos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Productos
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var productosModel = _mapper.Map<ProductosModel>(query);
                return View(productosModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // GET: Productos/Create
        public ActionResult Create()
        {
            if (SignInManager.IsSignedIn(User))
            {
                return View();
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Productos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Id,Descripcion,Precio")] ProductosModel productosModel)
        {
            if (ModelState.IsValid)
            {
                var productos = _mapper.Map<Productos>(productosModel);
                _context.Add(productos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(productosModel);
        }

        // GET: Productos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Productos.FindAsync(id);
                if (query == null)
                {
                    return NotFound();
                }
                var productosModel = _mapper.Map<ProductosModel>(query);
                return View(productosModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Productos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, [Bind("Id,Descripcion,Precio")] ProductosModel productosModel)
        {
            if (id != productosModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var productos = _mapper.Map<Productos>(productosModel);
                    _context.Update(productos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductosExists(productosModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(productosModel);
        }

        // GET: Productos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (SignInManager.IsSignedIn(User))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var query = await _context.Productos
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (query == null)
                {
                    return NotFound();
                }
                var productosModel = _mapper.Map<ProductosModel>(query);
                return View(productosModel);
            }
            else
            {
                return RedirectToAction(nameof(LoginController.Index), "Login");
            }
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var productos= await _context.Productos.FindAsync(id);
            _context.Productos.Remove(productos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductosExists(int id)
        {
            return _context.Productos.Any(e => e.Id == id);
        }
    }
}
